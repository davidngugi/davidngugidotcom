//This is the "Offline page" service worker
var cacheName = 'davidwebcachev1';
var cacheFiles =
[
    '/',
    '/index.php',
    '/manifest.json',
    '/browserconfig.xml',
    '/app_icons/android-chrome-192x192.png',
    '/app_icons/android-chrome-256x256.png',
    '/app_icons/apple-touch-icon.png',
    '/app_icons/favicon-16x16.png',
    '/app_icons/favicon-32x32.png',
    '/app_icons/favicon.ico',
    '/app_icons/mstile-150x150.png',
    '/app_icons/safari-pinned-tab.svg',
    '/assets_screenshots/ardhi.png',
    '/assets_screenshots/bujetly.png',
    '/assets_screenshots/davidngugi.jpg',
    '/assets_screenshots/haveystar.jpg',
    '/assets_screenshots/komahill.jpg',
    '/assets_screenshots/laricon.jpg',
    '/assets_screenshots/congress_trivia.png',
    '/assets_screenshots/devcontact.png',
    '/assets_screenshots/minesweeper_game.png',
    '/assets_screenshots/opengl_test.png',
    '/assets_screenshots/Phaser_test.png',
    '/assets_screenshots/thornbird.jpg',
    '/assets_screenshots/tukblocks.png',
    '/assets_screenshots/youtube_video_jungle.jpg',
    '/css/animate.css',
    '/css/bootstrap.css',
    '/css/bootstrap.min.css',
    '/css/owl.carousel.css',
    '/css/owl.theme.css',
    '/css/style.css',
    '/fns/contact.php',
    '/fonts/font-awesome/css/font-awesome.css',
    '/fonts/font-awesome/css/font-awesome.min.css',
    '/fonts/font-awesome/font/fontawesome-webfont.svg',
    '/fonts/font-awesome/font/fontawesome-webfont.ttf',
    '/fonts/font-awesome/font/fontawesome-webfont.woff',
    '/fonts/font-awesome/font/fontawesome-webfont.woff2',
    '/js/bootstrap.js',
    '/js/bootstrap.min.js',
    '/js/main.js',
    '/js/jquery.isotope.js',
    '/js/jquery.1.11.1.js',
    '/js/jquery.min.js',
    '/js/modernizr.custom.js',
    '/js/owl.carousel.js',
    '/js/SmoothScroll.js',
    '/img/01.jpg',
    '/img/02.jpg',
    '/img/banner.jpg',
    '/img/nairobi.jpg'
];

//Install stage sets up the offline page in the cache and opens a new cache
self.addEventListener('install', function(event) {
  event.waitUntil(
        caches.open(cacheName).then(function(cache) {
          console.log('[CACHE STORE] Cached offline page during Install');
          return cache.addAll(cacheFiles);
        })
    );
});

self.addEventListener('activate', function(event){
    console.info( '[CACHE STORE] Service Worker activated' );

    event.waitUntil(
        caches.keys().then(function(cacheNames){
            return Promise.all(cacheNames.map(function(thisCacheName){
                if(thisCacheName !== cacheName){
                    console.log("[CACHE STORE] Removing cached files from " + thisCacheName);
                    return caches.delete(thisCacheName);
                }
            }))
        })
    );
});

//If any fetch fails, it will show the offline page.
self.addEventListener('fetch', function(event) {
  event.respondWith(
      caches.match(event.request).then(function(response){
          if(response){
              console.info('[CACHE STORE] Found in cache ', event.request.url);
              return response;
          }

          var requestClone = event.request.clone();

          fetch(requestClone).then(function(response){
              if(!response){
                  console.error("[CACHE STORE] No response from fetch");
                  return response;
              }

              var responseClone = response.clone();

              caches.open(cacheName).then(function(cache) {
                console.log('[CACHE STORE] Cached offline page during fetch');
                cache.put(event.request, responseClone);
                return response;
              })

          })
          .catch(function(err){
               console.error("[CACHE STORE] Fetch and caching failed ", err);
          })
      })

    )
});
