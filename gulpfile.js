var gulp = require('gulp');
var sync = require('browser‐sync').create();

gulp.task('browser‐sync', function() {
    sync.init({
        server: { baseDir: "./" }
    });

    gulp.watch("/**/*.php").on('change', sync.reload);
    gulp.watch("app_icons/**/*.*").on('change', sync.reload);
    gulp.watch("assets_screenshots/**/*.*").on('change', sync.reload);
    gulp.watch("css/**/*.*").on('change', sync.reload);
    gulp.watch("fns/**/*.*").on('change', sync.reload);
    gulp.watch("fonts/**/*.*").on('change', sync.reload);
    gulp.watch("img/**/*.*").on('change', sync.reload);
    gulp.watch("js/**/*.*").on('change', sync.reload);
});
