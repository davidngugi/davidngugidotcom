<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>David Ngugi | Home of Awesomeness</title>
    <meta name="description" content="Personal/Portfolio website of David Ngugi. The home of web and software development">
    <meta name="keywords" content="David Ngugi wants to turn your app idea into reality, make your website accessible and fit for the 21st Century. Making software is David's passion, love and life career.">
    <meta name="author" content="David Ngugi Ndung'u">

    <!-- Favicons
    ================================================== -->

    <link rel="apple-touch-icon" sizes="180x180" href="app_icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="app_icons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="app_icons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="app_icons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#333">
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">

    <!-- Slider
    ================================================== -->
    <link href="css/owl.carousel.css" rel="stylesheet" media="screen">
    <link href="css/owl.theme.css" rel="stylesheet" media="screen">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">

    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="js/modernizr.custom.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NBF4G2"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NBF4G2');</script>
<!-- End Google Tag Manager -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74654720-1', 'auto');
  ga('require', 'linkid');
  ga('send', 'pageview');
  ga('set', 'userId', {{USER_ID}}); // Set the user ID using signed-in user_id.

</script>
    <!-- Navigation
    ==========================================-->
    <nav id="tf-menu" class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">David Ngugi</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#tf-home" class="page-scroll">Home</a></li>
            <li><a href="http://blog.davidngugi.com" class="page-scroll">Blog</a></li>
            <li><a href="#tf-about" class="page-scroll">About</a></li>
            <!--<li><a href="#tf-services" class="page-scroll">Services</a></li> -->
            <li><a href="#tf-works" class="page-scroll">Portfolio</a></li>
            <li><a href="#tf-contact" class="page-scroll">Contact</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <!-- Home Page
    ==========================================-->
    <div id="tf-home" class="text-center">
        <div class="overlay">
            <div class="content">
                <h1>Hi, I am<strong><span class="color"> David Ngugi</span></strong></h1>
                <p class="lead">Software Engineer, Blogger, Tech Enthusiast</p>
                <a href="#tf-about" class="fa fa-angle-down page-scroll"></a>
            </div>
        </div>
    </div>

    <!-- About Us Page
    ==========================================-->
    <div id="tf-about">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="img/02.jpg" class="img-responsive" width="80%" height="50%">
                </div>
                <div class="col-md-6">
                    <div class="about-text">
                        <div class="section-title">
                            <h2>Who is <strong>David Ngugi</strong>?</h2>
                            <hr>
                            <div class="clearfix"></div>
                        </div>
                        <p class="intro">David Ngugi is your Software Engineer of choice when it comes to bringing your dreams to reality. He is Kenyan-born and well educated. He's the full package of innovation and creativity! Look no further.</p>
                        <ul class="about-list">
                            <li>
                                <span class="fa fa-dot-circle-o"></span>
                                <strong>Mission</strong> - <em>To fulfill my purpose as a human being as I touch lives using my work.</em>
                            </li>
                            <li>
                                <span class="fa fa-dot-circle-o"></span>
                                <strong>Skills</strong> - <em>Software and web development using JavaScript (JQuery, NodeJs, AngularJS, VueJs), PHP (The Laravel Web Framework), Python, C#, HTML5 and CSS3.</em>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Services Section
    ==========================================-->


    <!-- Portfolio Section
    ==========================================-->
    <div id="tf-works">
        <div class="container"> <!-- Container -->
            <div class="section-title text-center center">
                <h2>Projects<strong> Done</strong></h2>
                <div class="line">
                    <hr>
                </div>
                <div class="clearfix"></div>
                <small><em>Some of the my personal and commercial projects that I've done over the years. Take note a great number are not online and may not be available for full review.</em></small>
            </div>
            <div class="space"></div>

            <div class="categories">

                <ul class="cat">
                    <li class="pull-left"><h4>Filter by Type:</h4></li>
                    <li class="pull-right">
                        <ol class="type">
                            <li><a href="#" data-filter="*" class="active">All</a></li>
                            <li><a href="#" data-filter=".web">Web Design</a></li>
                            <li><a href="#" data-filter=".dev" >Web Dev</a></li>
                            <li><a href="#" data-filter=".games" >Games</a></li>
                        </ol>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div id="lightbox" class="row">

                <div class="col-sm-6 col-md-4 col-lg-4 web">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="assets_screenshots/davidngugi.jpg">
                                <div class="hover-text">
                                    <h4>davidngugi.com</h4>
                                    <small>Web Design (HTML5,CSS3)</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="assets_screenshots/davidngugi.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4 dev">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="assets_screenshots/bujetly.png">
                                <div class="hover-text">
                                    <h4>Bujetly</h4>
                                    <small>Web App (PHP)</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="assets_screenshots/bujetly.png" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4 dev">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="assets_screenshots/devcontact.png">
                                <div class="hover-text">
                                    <h4>DevContact Social Network</h4>
                                    <small>Web App (PHP)</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="assets_screenshots/devcontact.png" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4 dev">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="assets_screenshots/tukblocks.png">
                                <div class="hover-text">
                                    <h4>TU-K Blocks</h4>
                                    <small>Web App (PHP)</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="assets_screenshots/tukblocks.png" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4 web">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="assets_screenshots/thornbird.jpg">
                                <div class="hover-text">
                                    <h4>Thornbirdtours.co.ke</h4>
                                    <small>Web Design (Wordpress)</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="assets_screenshots/thornbird.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4 web">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="assets_screenshots/haveystar.jpg">
                                <div class="hover-text">
                                    <h4>Haveystar.co.ke</h4>
                                    <small>Web Design (Wordpress)</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="assets_screenshots/haveystar.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4 web">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="assets_screenshots/komahill.jpg">
                                <div class="hover-text">
                                    <h4>Komahillbookshop.co.ke</h4>
                                    <small>Web Design (HTML5,CSS3,JS)</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="assets_screenshots/komahill.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4 web">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="assets_screenshots/laricon.jpg">
                                <div class="hover-text">
                                    <h4>Laricon.co.ke</h4>
                                    <small>Web Design (HTML5,CSS3,JS)</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="assets_screenshots/laricon.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4 dev">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="assets_screenshots/youtube_video_jungle.jpg">
                                <div class="hover-text">
                                    <h4>Youtube Video Jungle</h4>
                                    <small>M.E.A.N App</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="assets_screenshots/youtube_video_jungle.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4 dev">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="assets_screenshots/ardhi.png">
                                <div class="hover-text">
                                    <h4>ARDHI SLRS</h4>
                                    <small>Web App (Laravel)</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="assets_screenshots/ardhi.png" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4 games">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="assets_screenshots/minesweeper_game.png">
                                <div class="hover-text">
                                    <h4>Minesweeper</h4>
                                    <small>Game experiment (Javascript, CSS)</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="assets_screenshots/minesweeper_game.png" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4 games">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="assets_screenshots/Phaser_test.png">
                                <div class="hover-text">
                                    <h4>Phaser game</h4>
                                    <small>JavaScript with PhaserJs</small>
                                    <div class="clearfix"></div>
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="assets_screenshots/Phaser_test.png" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>

    <!-- Contact Section
    ==========================================-->
    <div id="tf-contact" class="text-center">
        <div class="container">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <div class="section-title center">
                        <h2>Say Hi to me <strong>here</strong></h2>
                        <div class="line">
                            <hr>
                        </div>
                        <div class="clearfix"></div>
                        <small><em></em></small>
                    </div>
                    <?php include_once("fns/contact.php");?>
                    <form action = "<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
                        <input type="hidden" name="_token" value = "<?php echo sha1(rand(0,15));?>" required>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="full_name">Full Name</label>
                                    <input type="text" class="form-control" name="full_name" placeholder="Enter Your Full Name" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="email">Email address</label>
                                    <input type="email" class="form-control" name="email" placeholder="Enter email" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="subject">Subject</label>
                                    <input type="subject" class="form-control" name="subject" placeholder="Enter Subject" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea class="form-control" name = "message" rows="3"></textarea>
                        </div>

                        <button type="submit" name = "send" class="btn tf-btn btn-default">Send</button>
                    </form>
                     <div class="row col-md-12" style="padding-top: 5%;">
                <br><br>
                    <a class="btn btn-block btn-primary" style="font-size: 1.2em;"><i class="fa fa-2x fa-whatsapp"></i> WhatsApp me at +254 705-769-370</a>
                </div>
                <div class = "row">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Dave Ads -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-2064241763088830"
                         data-ad-slot="9212727501"
                         data-ad-format="auto"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>

                </div>

        </div>
    </div>

    <nav id="footer">
        <div class="container">
            <div class="pull-left fnav">
                <p>COPYRIGHT &copy; <?php echo date('Y');?> DAVID NGUGI. ALL RIGHTS RESERVED.</p>
            </div>
            <div class="pull-right fnav">
                <ul class="footer-social">
                    <li><a href="https://www.facebook.com/david.ngugi.334" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://www.twitter.com/DavidNgugi15" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.github.com/DavidNgugi" title="Github"><i class="fa fa-github"></i></a></li>
                    <li><a href="#tf-home" class="page-scroll">&nbsp;&nbsp;<i class="fa fa-arrow-up"></i>&nbsp;Back To Top</a></li>
                </ul>

            </div>

        </div>
    </nav>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
    <script type="text/javascript" src="js/jquery.1.11.1.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/SmoothScroll.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.js"></script>

    <script src="js/owl.carousel.js"></script>

    <!-- Javascripts
    ================================================== -->
    <script type="text/javascript" src="js/main.js"></script>
    <script type="text/javascript">
    	if (navigator.serviceWorker.controller) {
    		console.info('[CACHE STORE] active service worker found, no need to register')
    	} else {
    	//Register the ServiceWorker
    	navigator.serviceWorker.register('sw.js', {
    	scope: './'
    	})
    	.then(function(reg) {
    		console.log('Service worker has been registered for scope:'+ reg.scope);
    	})
    	.catch(function(err){
    		console.error('Service worker failed to register' + err);
    	});
    }
    </script>
  </body>
</html>
